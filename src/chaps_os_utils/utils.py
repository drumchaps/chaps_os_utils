#-*- coding: utf-8 -*-

import getpass
import os
import platform
import ctypes

def get_user():
    """Gets the current user.\
    """
    return getpass.getuser()


def get_current_dir():
    try:
        thisdir = os.path.dirname(os.path.realpath(__file__))
    except NameError:
        thisdir = os.path.dirname(ps.path.abspath(sys.argv[0]))
    return thisdir

def is_windows():
    """ Checks wether the running platform is a Windows System.\
    """
    return platform.system() == "Windows"
    pass


def is_linux():
    """ Checks wether the running platform is a linux system.\
    """
    return platform.system() == "Linux"


def is_admin():
    try:
        return os.getuid() == 0
    except AttributeError:
        return ctypes.windll.shell32.IsUserAnAdmin() != 0
